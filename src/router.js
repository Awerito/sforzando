import Vue from "vue"
import Router from "vue-router"
import Home from "./views/Home.vue"
import Docs from "./views/Docs.vue"

Vue.use(Router)

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: Home
    },
    {
      path: "/docs",
      component: Docs,
      children: [ 
        {
	  path: "",
          component: () => import(/* webpackChunkName: "docs-basics" */ "./components/DocsBasics.vue")
        }, 
        {
	  path: "lorem",
          component: () => import(/* webpackChunkName: "docs-lorem" */ "./components/DocsLorem.vue")
        } 
      ]
    },
  ]
})
