import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import "./registerServiceWorker"
import "bootstrap/dist/css/bootstrap.min.css"
import "jquery/src/jquery.js"
import "bootstrap/dist/js/bootstrap.min.js"
import "bootstrap-icons/font/bootstrap-icons.css"

import BasePiano from "./components/BasePiano.vue"
import Navbar from "./components/Navbar.vue"
import Footer from "./components/Footer.vue"
import Piano from "./components/Piano.vue"
import Chord from "./components/Chord.vue"
import Notes from "./components/Notes.vue"
import Timer from "./components/Timer.vue"

Vue.component("navigation", Navbar)
Vue.component("board", BasePiano)
Vue.component("piano", Piano)
Vue.component("foot", Footer)
Vue.component("chord", Chord)
Vue.component("notes", Notes)
Vue.component("timer", Timer)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app")
